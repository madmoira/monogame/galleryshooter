﻿using System;

using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace shootingGallery
{
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;

        Texture2D crosshairsSprite;
        const int CROSSHAIR_SIZE = 25;

        Texture2D backgroundSprite;
        SpriteFont uiFont;

        Texture2D targetSprite;
        Vector2 targetPosition = new Vector2(300, 300);
        const int TARGET_RADIUS = 45;

        MouseState mState;
        Vector2 mPosition = new Vector2();
        bool mReleased = true;
        float mouseTargetDist;

        int score = 0;
        float timer = 10f;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
            IsMouseVisible = false;
        }

        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);

            targetSprite = Content.Load<Texture2D>("target");
            crosshairsSprite = Content.Load<Texture2D>("crosshairs");
            backgroundSprite = Content.Load<Texture2D>("sky");

            uiFont = Content.Load<SpriteFont>("galleryFont");
        }

        protected override void UnloadContent()
        {
            base.UnloadContent();
        }

        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            if (timer > 0)
            {
                timer -= (float)gameTime.ElapsedGameTime.TotalSeconds;
            }

            mState = Mouse.GetState();
            mPosition.X = mState.X;
            mPosition.Y = mState.Y;

            mouseTargetDist = Vector2.Distance(targetPosition, mPosition);

            if (mState.LeftButton == ButtonState.Pressed && mReleased == true)
            {
                if (mouseTargetDist < TARGET_RADIUS && timer > 0)
                {
                    score += 1;
                    Random rand = new Random();
                    targetPosition.X = rand.Next(TARGET_RADIUS, graphics.PreferredBackBufferWidth - TARGET_RADIUS);
                    targetPosition.Y = rand.Next(TARGET_RADIUS, graphics.PreferredBackBufferHeight - TARGET_RADIUS);
                }
                mReleased = false;
            }

            if (mState.LeftButton == ButtonState.Released)
            {
                mReleased = true;
            }

            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            spriteBatch.Begin();

            spriteBatch.Draw(backgroundSprite, new Vector2(0, 0), Color.White);
            if (timer > 0) spriteBatch.Draw(targetSprite, new Vector2(targetPosition.X - TARGET_RADIUS, targetPosition.Y - TARGET_RADIUS), Color.White);
            spriteBatch.DrawString(uiFont, $"Score: {score.ToString()}", new Vector2(3, 3), Color.White);
            spriteBatch.DrawString(uiFont, $"Time: {Math.Ceiling(timer).ToString()}", new Vector2(3, 40), Color.AliceBlue);

            // Crosshair
            spriteBatch.Draw(crosshairsSprite, new Vector2(mState.X - CROSSHAIR_SIZE, mState.Y - CROSSHAIR_SIZE), Color.White);

            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}